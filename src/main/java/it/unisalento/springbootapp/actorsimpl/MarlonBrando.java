package it.unisalento.springbootapp.actorsimpl;

import it.unisalento.springbootapp.iactors.IActor;

public class MarlonBrando implements IActor {

	public void speak() {
		System.out.println("I'm gonna make him a offer he cannot refuse!!!");
	}
	
}
