package it.unisalento.springbootapp.dto;

import org.springframework.web.multipart.MultipartFile;

public class AttachmentDTO {
	
	int id;
	MultipartFile file;
	double lat;
	double lon;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
	
	
	
	
}
