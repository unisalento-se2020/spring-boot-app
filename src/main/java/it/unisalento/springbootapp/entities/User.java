package it.unisalento.springbootapp.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String name;
	String surname;
	String type;
	String email;
	String password;
	
	@OneToMany(mappedBy = "user")
	List<InterventionNote> interventionNotesList;
	
	
	@OneToMany(mappedBy = "user")
	List<UserWarning> userWarningList;
	
	
	/* How you can implement directly a MANYTOMANY relationship
	 * SEE in the warning class
	@ManyToMany(mappedBy = "userList")
	List<Warning> warningList;
	*/
 	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<InterventionNote> getInterventionNotesList() {
		return interventionNotesList;
	}
	public void setInterventionNotesList(List<InterventionNote> interventionNotesList) {
		this.interventionNotesList = interventionNotesList;
	}
	public List<UserWarning> getUserWarningList() {
		return userWarningList;
	}
	public void setUserWarningList(List<UserWarning> userWarningList) {
		this.userWarningList = userWarningList;
	}
	
	
	
}
