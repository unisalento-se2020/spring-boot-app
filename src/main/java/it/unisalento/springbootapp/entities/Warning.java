package it.unisalento.springbootapp.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Warning {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	String description;
	String type;
	
	@OneToOne
	InterventionNote interventionNote;
	
	/** How you can implement directly a MANYTOMANY relationship
	@ManyToMany
	List<User> userList;
	**/
	
	@OneToMany(mappedBy = "warning")
	List<UserWarning> userWarningList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public List<UserWarning> getUserWarningList() {
		return userWarningList;
	}

	public void setUserWarningList(List<UserWarning> userWarningList) {
		this.userWarningList = userWarningList;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public InterventionNote getInterventionNote() {
		return interventionNote;
	}

	public void setInterventionNote(InterventionNote interventionNote) {
		this.interventionNote = interventionNote;
	}
	
	
	
}
