package it.unisalento.springbootapp.iactors;

public interface IActor {
	
	public void speak();
	
}
