package it.unisalento.springbootapp.iservices;

import java.util.List;

import it.unisalento.springbootapp.entities.User;
import it.unisalento.springbootapp.execptions.UserNotFoundException;

public interface IUserService {
	
	User save (User user);
	List<User> getAll();
	User getById (int id) throws UserNotFoundException;
	void delete (int id) throws UserNotFoundException;
	List<User> getByName(String name);

}
