package it.unisalento.springbootapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unisalento.springbootapp.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	List<User> findByName(String name);
	
	@Query("select a from User a where a.name=:name")
	List<User> findByNameUsingAQuery(@Param(value = "name") String name);
	
	
	List<User> findByNameAndSurname(String name, String surname);
	
	
	@Query("select a from User a where a.name=:name and a.surname=:surname")
	List<User> findByNameAndSurnameUsingAQuery(@Param(value="name")String name,@Param("surname") String surname);
	
	
	
}
