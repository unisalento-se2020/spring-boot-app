package it.unisalento.springbootapp.restcontrollers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.unisalento.springbootapp.dto.AttachmentDTO;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentRestController {

	private static String UPLOADED_FOLDER = "/Users/alessandro/uploads/";
	
	@PostMapping("/uploadFile")
	public ResponseEntity<?> upload(@RequestAttribute("file") MultipartFile file) throws IOException {
		
		System.out.println("fileName:"+file.getOriginalFilename());
		saveFile(file);
		
		return new ResponseEntity("OK", HttpStatus.OK);
	}
	
	
	@PostMapping("/uploadFileAndInfo")
	public ResponseEntity<?> uploadWithInfo(@ModelAttribute AttachmentDTO attachment) throws IOException {
		
		saveFile(attachment.getFile());
		
		
		return new ResponseEntity("OK", HttpStatus.OK);
	}
	
	
	private void saveFile(MultipartFile file) throws IOException {
		byte[] bytes = file.getBytes();
		String filename = generateUID()+file.getOriginalFilename();
        Path path = Paths.get(UPLOADED_FOLDER + filename);
        Files.write(path, bytes);
	}
	
	private String generateUID () {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
}
