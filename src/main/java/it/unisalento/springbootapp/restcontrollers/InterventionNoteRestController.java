package it.unisalento.springbootapp.restcontrollers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.springbootapp.dto.InterventionNoteDTO;

@RestController
@RequestMapping("/api/interventionNote")
public class InterventionNoteRestController {
	
	@PostMapping(
			value="/save",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public InterventionNoteDTO post(@RequestBody InterventionNoteDTO interventionNoteDTO) {
		
		
		return interventionNoteDTO;
	}

}
