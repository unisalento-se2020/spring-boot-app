package it.unisalento.springbootapp.singerimpl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import it.unisalento.springbootapp.isinger.ISinger;

@Component
@Qualifier
public class Bowie implements ISinger {

	@Override
	public void sing() {
		// TODO Auto-generated method stub
		System.out.println("We can be heroes just one day!!!");
	}

}
